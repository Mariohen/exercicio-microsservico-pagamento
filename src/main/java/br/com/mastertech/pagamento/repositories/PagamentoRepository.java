package br.com.mastertech.pagamento.repositories;

import br.com.mastertech.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    List<Pagamento> findByCartaoId(int cartaoId);

}

