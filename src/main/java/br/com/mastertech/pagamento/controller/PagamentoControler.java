package br.com.mastertech.pagamento.controller;

import br.com.mastertech.pagamento.clienteCartao.CartaoPagamento;
import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.services.CartaoNotFoundException;
import br.com.mastertech.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoControler {

    @Autowired
    private CartaoPagamento cartaoPagamento;

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento cadastrarPagamento(@RequestBody @Valid Pagamento pagamento) {
        cartaoPagamento.getById(pagamento.getCartaoId());
        Pagamento objetoPagamento = pagamentoService.salvarPagamento(pagamento);
        return objetoPagamento;

    }

    @GetMapping("/{id}")
    public List<Pagamento> pesquisaPorCartaoId(@RequestParam(name = "cartaoId") int cartaoId){
        return pagamentoService.pesquisarPorCartaoID(cartaoId);
    }


}
