package br.com.mastertech.pagamento.services;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Sistema inoperante!!!")
public class CartaoFallbackException extends RuntimeException {
}
