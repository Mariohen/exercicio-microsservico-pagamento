package br.com.mastertech.pagamento.services;

import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento salvarPagamento(Pagamento pagamento){
            Pagamento objetoPagamento = pagamentoRepository.save(pagamento);
            return objetoPagamento;
    }


    //retorno de todos os registros
    public Iterable<Pagamento> lerTodosOsProdutos(){
        return pagamentoRepository.findAll();
    }


    public List<Pagamento> pesquisarPorCartaoID(int cartaoId){
        return pagamentoRepository.findByCartaoId(cartaoId);

    }

    public List<Pagamento> buscarFatura(Integer clienteId, Integer cartaoId){
                return pagamentoRepository.findByCartaoId(cartaoId);
    }

}
