package br.com.mastertech.pagamento.clienteCartao;

import br.com.mastertech.pagamento.services.CartaoNotFoundException;
import feign.codec.ErrorDecoder;
import feign.Response;

public class CartaoPagamentoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
