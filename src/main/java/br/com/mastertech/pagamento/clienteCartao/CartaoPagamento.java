package br.com.mastertech.pagamento.clienteCartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoPagamentoConfiguracao.class)
public interface CartaoPagamento {

    @GetMapping("/cartao/{id}")
    Cartao getById(@PathVariable Integer id);

}
