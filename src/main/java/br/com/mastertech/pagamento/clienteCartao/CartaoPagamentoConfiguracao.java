package br.com.mastertech.pagamento.clienteCartao;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoPagamentoConfiguracao {
    @Bean
    public ErrorDecoder buscarCartao() {
        return new CartaoPagamentoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoPagamentoFallback(), RetryableException.class)
                .withFallbackFactory(CartaoPagamentoLoadBalanceConfiguracao::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }


}
