package br.com.mastertech.pagamento.clienteCartao;

import br.com.mastertech.pagamento.services.CartaoFallbackException;

public class CartaoPagamentoFallback implements CartaoPagamento{
    @Override
    public Cartao getById(Integer id) {
      throw new CartaoFallbackException();
    }
}
