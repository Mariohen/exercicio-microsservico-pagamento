package br.com.mastertech.pagamento.clienteCartao;

import br.com.mastertech.pagamento.services.CartaoNotFoundException;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoPagamentoLoadBalanceConfiguracao implements CartaoPagamento {

    public CartaoPagamentoLoadBalanceConfiguracao(Exception exception) {
        this.exception = exception;
    }

    private Exception exception;

    @Override
    public Cartao getById(Integer id) {
        if(exception.getCause() instanceof CartaoNotFoundException) {
            Cartao cartao = new Cartao();
            return cartao;
        }
        throw (RuntimeException) exception;
    }

}
