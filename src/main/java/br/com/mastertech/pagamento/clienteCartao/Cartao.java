package br.com.mastertech.pagamento.clienteCartao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Cartao {

    private Integer id;
    private String numero;
    private  Integer clienteId;
    private boolean ativo;

    public Cartao() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
